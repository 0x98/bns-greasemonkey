/**
 * This script takes your package.json info and prepends the necessary header comments to the userscript.
 * You'll have to add your @match url and @include if needed (below an example)
 *
 * // @match        https://a-cool-website.com
 * // @include      /someRegExOrOtherUrl/
 */

const fs = require('fs');
const package = require('./package.json');

const baseUrl = 'https://gitlab.com/0x98/bns-greasemonkey';
const distUserScript = package.name + '.user.js';
const updateUrl = `${baseUrl}/-/raw/master/dist/${distUserScript}`;
const downloadUrl = updateUrl;

const HEADER = `// ==UserScript==
// @name         ${package.name}
// @namespace    ${baseUrl}
// @version      ${package.version}
// @description  ${package.description}
// @licence      ${package.license}
// @author       ${package.author}
// @match        *://etherscan.io/*
// @match        *://cn.etherscan.com/*
// @match        *://bscscan.com/*
// @match        *://snowtrace.io/*
// @match        *://polygonscan.com/*
// @match        *://dune.com/*
// @grant        GM_getResourceText
// @grant        GM_addStyle
// @grant        GM_xmlhttpRequest
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_listValues
// @grant        GM_deleteValue
// @grant        GM_registerMenuCommand
// @grant        GM_setClipboard
// @require      https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js
// @require      https://cdn.jsdelivr.net/npm/jquery-ui-dist@1.13.1/jquery-ui.min.js
// @resource     jqueryUIStyle https://cdn.jsdelivr.net/npm/jquery-ui-dist@1.13.1/jquery-ui.min.css
// @updateURL    ${updateUrl}
// @downloadURL  ${downloadUrl}
// ==/UserScript==

`;

/**
 * It's dangerous to go alone, make sure to be well equiped if you want to fiddle with this code ;D
 */

const data = fs.readFileSync('./dist/' + distUserScript);
const fd = fs.openSync('./dist/' + distUserScript, 'w+');
const insert = new Buffer.alloc(HEADER.length, HEADER);

fs.writeSync(fd, insert, 0, insert.length, 0);
fs.writeSync(fd, data, 0, data.length, insert.length);
fs.close(fd, err => {
  if (err) {
    throw err;
  } else {
    console.info('Successfully added the header to the userscript !');
  }
});
