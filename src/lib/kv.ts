// @ts-nocheck

import * as _ from 'lodash';
import { now } from './time';

export async function kvSet(key: string, data: any, ttl?: number) {
  await GM_setValue(
    key,
    JSON.stringify({ expire: ttl ? now() + ttl : 0, data }),
  );
}

export async function kvGet<T = any>(key: string): Promise<T | undefined> {
  const result = JSON.parse((await GM_getValue(key)) || '{}');
  if (result.expire && now() > result.expire) {
    return undefined;
  }
  return result.data as T;
}

export async function kvClean(prefix: string) {
  const values = await GM_listValues();
  for (const v of values) {
    if (!_.startsWith(v, prefix)) continue;
    GM_deleteValue(v);
  }
}
