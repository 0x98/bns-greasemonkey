// @ts-nocheck

import * as qs from 'qs';

export async function httpGet<T = any>(
  url: string,
  query: any = {},
): Promise<T> {
  return new Promise<T>((resolve, reject) => {
    GM_xmlhttpRequest({
      method: 'GET',
      url: url + '?' + qs.stringify(query),
      onload: function (rsp) {
        const result = JSON.parse(rsp.responseText || '{}') as T;
        resolve(result);
      },
      onerror: function (err) {
        reject(err);
      },
    });
  });
}
