// @ts-nocheck

export function addMenu(name: string, func: () => Promise<void>) {
  GM_registerMenuCommand(name, func);
}

GM_registerMenuCommand('Clean Cache', async () => {
  const values = await GM_listValues();
  for (const v of values) {
    if (!v.includes('0x98_label_')) continue;
    GM_deleteValue(v);
  }
});
