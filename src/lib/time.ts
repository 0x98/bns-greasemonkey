// unixtime in millisecond
export function nowInMs(): number {
  return Math.round(new Date().getTime());
}

// unixtime in second
export function now(): number {
  return Math.round(new Date().getTime() / 1000);
}

export async function sleep(ms: number) {
  return new Promise((resolve, _reject) => setTimeout(resolve, ms));
}

export async function loopSleep() {
  while (true) {
    await sleep(60_000);
  }
}
