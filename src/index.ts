import './css/style.css';

import * as _ from 'lodash';

import { kvGet, kvSet, kvClean } from './lib/kv';
import { httpGet } from './lib/http';
import { sleep } from './lib/time';
import { addMenu } from './lib/menu';

const apiUrl = 'https://0x98.bns.name';
const ethAddresssReg = /^0x[a-fA-F0-9]{40}$/;
const cacheTTL = 60 * 60 * 24 * 7; // seconds
const cachePrefix = '0x98_label_';

// @ts-ignore
GM_addStyle(GM_getResourceText('jqueryUIStyle'));

export class App {
  constructor() {
    console.log('bns user script running');
    this.run();
    addMenu('Clean Cache', () => kvClean(cachePrefix));
  }

  copyToClipboard = (content: string) => {
    const data = [
      new ClipboardItem({
        'text/plain': new Blob([content], { type: 'text/plain' }),
      }),
    ];
    navigator.clipboard.write(data);
  };

  getAddress(el: any, disableParent = false): string {
    const $el = $(el);
    const html = $el.html();
    if (!html || !html.includes('0x')) {
      return disableParent ? '' : this.getAddress($el.parent(), true);
    }

    let address = html;
    let htmls: string[] = [];

    if (html.includes('"0x')) {
      htmls = html.match(/"(0x.+)"/g);
    } else if (html.includes('>0x')) {
      htmls = html.match(/>(0x.+)</g);
    } else if (html.includes('(0x')) {
      htmls = html.match(/\((0x.+)\)/g);
    }
    if (htmls.length > 0) {
      address = htmls[htmls.length - 1]
        .replace('"', '')
        .replace('"', '')
        .replace('>', '')
        .replace('<', '')
        .replace('(', '')
        .replace(')', '');
    }

    address = address.toLowerCase();
    return ethAddresssReg.test(address) ? address : '';
  }

  setAddressLabel(el: HTMLElement, labels: any, addTitle = false) {
    const address = this.getAddress(el);
    if (!address || !labels[address]) return;

    const $el = $(el);
    const html = $el.html();

    if (!html.includes('<') || !html.includes('>')) {
      $el.html(labels[address]);
    } else if (addTitle) {
      // @ts-ignore
      $el
        .css('cursor', 'pointer')
        .on('click', () => {
          // @ts-ignore
          GM_setClipboard(address);
        })
        // @ts-ignore
        .tooltip()
        .html(
          html.replace(
            />[^<]+</,
            ` title="点击复制: ${address}">${labels[address]}<`,
          ),
        );
    } else {
      $el.html(html.replace(/>[^<]+</, `>${labels[address]}<`));
    }
  }

  fromEtherscan(url: string):
    | {
        loop: boolean;
        network: string;
        selectors: string[];
        addTitle: boolean;
      }
    | undefined {
    let network = '';
    if (url.includes('//etherscan.') || url.includes('//cn.etherscan.')) {
      network = 'eth';
    }
    if (url.includes('//bscscan.')) {
      network = 'bsc';
    }
    if (url.includes('//snowtrace.')) {
      network = 'avax';
    }
    if (url.includes('//polygonscan.')) {
      network = 'matic';
    }
    return network.length > 0
      ? {
          loop: false,
          network,
          selectors: [
            '.hash-tag',
            '#addressCopy',
            '#contractCopy',
            'div.media-body > span > a > span', // .../tx/0x1234 -> Transaction Action
            'table > tbody > tr > td:nth-child(2) > a', // .../gastracker
          ],
          addTitle: false,
        }
      : undefined;
  }

  fromDune(url: string):
    | {
        loop: boolean;
        network: string;
        selectors: string[];
        addTitle: boolean;
      }
    | undefined {
    let network = '';
    if (url.includes('dune.com')) {
      network = 'eth';
    }
    return network.length > 0
      ? {
          loop: true,
          network,
          selectors: ['table > tbody > tr > td > div'],
          addTitle: true,
        }
      : undefined;
  }

  async run() {
    const url = window.location.href;
    const { loop, network, selectors, addTitle } =
      this.fromEtherscan(url) || this.fromDune(url) || {};

    if (!network || network.length === 0) {
      return;
    }

    while (true) {
      const addresses: string[] = [];
      for (const slor of selectors) {
        $(slor).map((_, el) => {
          const address = this.getAddress(el);
          if (!address || addresses.includes(address)) return;
          addresses.push(address);
        });
      }

      const cachedLabels: any = {};
      const addressesNoCached: string[] = [];
      const getCachefuncs = [];
      for (const addr of addresses) {
        getCachefuncs.push(
          (async () => {
            return await kvGet<string>(`${cachePrefix}${network}_${addr}`);
          })(),
        );
      }
      const addressesCachedLabels = await Promise.all(getCachefuncs);
      for (let i = 0; i < addressesCachedLabels.length; i++) {
        if (addressesCachedLabels[i]?.length > 0) {
          cachedLabels[addresses[i]] = addressesCachedLabels[i];
        } else {
          addressesNoCached.push(addresses[i]);
        }
      }

      for (const slor of selectors) {
        $(slor).map((_, el) =>
          this.setAddressLabel(el, cachedLabels, addTitle),
        );
      }

      interface RootRsp {
        [key: string]: string | null;
      }

      const remoteLabels = await httpGet<RootRsp>(`${apiUrl}/labels`, {
        network,
        addresses: addressesNoCached.join(','),
      });

      for (const slor of selectors) {
        $(slor).map((_, el) =>
          this.setAddressLabel(el, remoteLabels, addTitle),
        );
      }

      const setCachefuncs = [];
      for (const addr of Object.keys(remoteLabels)) {
        if (!remoteLabels[addr]) continue;
        setCachefuncs.push(
          (async () => {
            await kvSet(
              `${cachePrefix}${network}_${addr}`,
              remoteLabels[addr],
              cacheTTL,
            );
          })(),
        );
      }
      await Promise.all(setCachefuncs);

      if (!loop) break;

      await sleep(10_000);
    }
  }
}

new App();
