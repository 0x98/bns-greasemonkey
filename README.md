## 安装教程

### 根据自己使用的浏览器安装 "暴力猴" 插件

- [Chrome Web Store](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag)
- [Edge Add-ons](https://microsoftedge.microsoft.com/addons/detail/violentmonkey/eeagobfjdenkkddmbclomhiblgggliao)

### 安装脚本

[点击开始安装](https://gitlab.com/0x98/bns-greasemonkey/-/raw/master/dist/bns.user.js)

## 插件效果

这是一个 ETH 链上的 [套利交易](https://etherscan.io/tx/0x69d9c4749c42436fdb6103c52d3d0d285af924f67dbf0b5e096138622436a7cd):

![](./docs/etherscan.png)

这是一个 BSC 链上的 [套利交易](https://bscscan.com/tx/0x496b369a4af6e579033e8f5fbe0e2b3fc3835f7f283c35cf6e7fc78089e0e78a):

![](./docs/bscscan.png)

这是一个 Dune 数据看板:

![](./docs/dune.png)
